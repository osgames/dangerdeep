# Danger from the Deep

Originally developed at https://sourceforge.net/projects/dangerdeep/ by [simsoueu](http://sourceforge.net/u/simsoueu/), [thjordan](http://sourceforge.net/u/thjordan/) and [tx2rx](http://sourceforge.net/u/tx2rx/) and published under the GPL-2.0 license.